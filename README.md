# ci-wechat-sdk
CodeIgniter-wechat-sdk
## 环境支持
[1] php >= 5.6

[2] CodeIgniter 3.x

## 使用教程

Wechat文件夹放application/libraries目录下

## 使用例子
```
<?php 

class SanLing extends CI_Controller
{
	public function __construct() {
		parent::__construct();

		# 配置参数

		$config = array(
			'appid'        => '', //公众号appid
			'partnerkey'   => '', //商户支付密钥Key
			'appsecret'    => '', //公众号appsecret
			'mch_id'       => '', //商户身份ID
		);
		
		# 加载对应操作接口

		//文件夹名注意大写
		$this->load->library('Wechat/Wechat_oauth',$config);
		$this->load->library('Wechat/wechat_pay',$config);
	}

	public function index()
	{
		//获取用户信息
		$getOauth = $this->wechat_oauth->getOauthAccessToken();

		$openid       = $getOauth['openid'];
		$body         = '支付';
		$out_trade_no = date('YmdHis');
		$total_fee    = 1;
		$notify_url   = 'http://www.30pu.net/notify';

		$this->wechat_pay->getPrepayId($openid, $body, $out_trade_no, $total_fee, $notify_url, $trade_type = "JSAPI");

		$this->wechat_pay->createMchPay($info);

	}

	public function getOauthRedirect()
	{
		$redirect_uri =  $this->wechat_oauth->getOauthRedirect('http:'.base_url('test/index'),time());
		redirect($redirect_uri);
	}
}
```
##获取当前完整页面(推荐写在helper中)
```
if (!function_exists('getLocalUrl'))
{
	function getLocalUrl() {
        $sys_protocal = isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443' ? 'https://' : 'http://';
        $php_self = $_SERVER['PHP_SELF'] ? $_SERVER['PHP_SELF'] : $_SERVER['SCRIPT_NAME'];
        $path_info = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '';
        $relate_url = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : $php_self.(isset($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : $path_info);
        return $sys_protocal.(isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '').$relate_url;
    }
}
```